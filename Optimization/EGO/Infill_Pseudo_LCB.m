function LCB = Infill_Pseudo_LCB(x, kriging_model_sur, kriging_model_var,opt)
%--------------------------------------------------------------------------
% the Upper Confidence Boud criterion
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% get the Kriging prediction and variance
[y,~] = predictor(x, kriging_model_sur);

[~,mse] = predictor(x, kriging_model_var);
s=sqrt(max(0,mse));

% calcuate the EI value
LCB=y-sqrt(opt.alpha).*s;



end





