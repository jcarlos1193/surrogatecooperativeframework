%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% matriz de gram entre X y Z
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function K=kernelX_Z(X,Z,theta,correlacion,escala)
% escalado de los datos
escalar = @(x) (x - repmat(escala(1,:),size(x,1),1)) ./ repmat(escala(2,:),size(x,1),1);
X=escalar(X);
Z=escalar(Z);
% Emplea los nucleos de DACEFIT
n=size(X,1);
m=size(Z,1);
evalc(['k = @(theta,d)',correlacion,'(theta,d);' ]);
d=[];
for i=1:n
    for j=1:m
        d(end+1,:)=X(i,:)-Z(j,:);
    end
end
r=k(theta,d); % calcula el vector de correlaciones
K=reshape(r,m,n);
end

