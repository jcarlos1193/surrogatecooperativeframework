function out=InformationMatrix(X,y,theta,correlacion,escala,c)
K=kernelX_Z(X,X,theta,correlacion,escala);
ymax=max(y);
ymin=min(y);
y=normalize(y,'range');
n=size(X,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INICIALIZACION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SIGMA=c*eye(n);
SIGMA_inv=(1/c)*eye(n);
SIGMA_det=c^n;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii=1:n
     K_i=K(:,ii)*(1-y(ii));
    [SIGMA_inv,SIGMA_det]=Sherman_Morrison(SIGMA,SIGMA_inv,K_i,SIGMA_det);
    SIGMA=SIGMA+K_i*K_i';  
end
out.SIGMA=SIGMA;
out.SIGMA_inv=SIGMA_inv;
out.SIGMA_det=SIGMA_det;
out.ymax=ymax;
out.ymin=ymin;
out.X=X;
end

