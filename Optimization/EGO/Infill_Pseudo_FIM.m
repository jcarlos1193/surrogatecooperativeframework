function obj = Infill_Pseudo_FIM(x, kriging_model, point_added,opt,F)


%--------------------------------------------------------------------------
% Fisher Information Matrix criterion
% F contiene todos los datos acumulados de la matriz de Fisher
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% get the Kriging prediction and variance
% get the Kriging prediction and variance
[y,mse] = predictor(x,kriging_model);
s=sqrt(max(0,mse));

%     % calcuate the FEI value

SIGMA=F.SIGMA;
SIGMA_inv=F.SIGMA_inv;
SIGMA_det=F.SIGMA_det;
ymax=F.ymax;
ymin=F.ymin;
X=F.X;
%%%%%%%%%%%%%%%%%

K_i=((ymax-y)./(ymax-ymin))'.*(kernelX_Z(X,x,kriging_model.theta,opt.ker,kriging_model.Ssc)');
for ii=1:size(K_i,2)
    [SIGMA_inv_aux,SIGMA_det_aux]=Sherman_Morrison(SIGMA,SIGMA_inv,K_i(:,ii),SIGMA_det);
    FIM(ii)=-log(SIGMA_det_aux);
end

%--------------------------------------------------------------------------
% if this is not the first infill point
if ~isempty(point_added)
    % the scaling of x
    scaled_x = (x - repmat(kriging_model.Ssc(1,:),size(x,1),1)) ./ repmat(kriging_model.Ssc(2,:),size(x,1),1);
    scaled_point_added = (point_added - repmat(kriging_model.Ssc(1,:),size(point_added,1),1)) ./ repmat(kriging_model.Ssc(2,:),size(point_added,1),1);
    correlation = zeros(size(scaled_x,1),size(scaled_point_added,1));
    for ii =1:size(scaled_point_added,1)
        dx = scaled_x - repmat(scaled_point_added(ii,:),size(x,1),1);
        correlation(:,ii) = feval(kriging_model.corr, kriging_model.theta, dx);
    end
    % the Pseudo EI matrix
    FIM = FIM.*prod(1-correlation,2);
end
% the objective needs to be maximized
obj = FIM';

end





