function obj = Infill_Pseudo_UCB(x, kriging_model, f_min, point_added,opt)
%--------------------------------------------------------------------------
% the Upper Confidence Boud criterion
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% get the Kriging prediction and variance
[y,mse] = predictor(x,kriging_model);
s=sqrt(max(0,mse));

%     % calcuate the EI value
UCB=y-sqrt(opt.alpha).*s;

%--------------------------------------------------------------------------
% if this is not the first infill point
if ~isempty(point_added)
    % the scaling of x
    scaled_x = (x - repmat(kriging_model.Ssc(1,:),size(x,1),1)) ./ repmat(kriging_model.Ssc(2,:),size(x,1),1);
    scaled_point_added = (point_added - repmat(kriging_model.Ssc(1,:),size(point_added,1),1)) ./ repmat(kriging_model.Ssc(2,:),size(point_added,1),1);
    correlation = zeros(size(scaled_x,1),size(scaled_point_added,1));
    for ii =1:size(scaled_point_added,1)
        dx = scaled_x - repmat(scaled_point_added(ii,:),size(x,1),1);
        correlation(:,ii) = feval(kriging_model.corr, kriging_model.theta, dx);
    end
    % the Pseudo EI matrix
    UCB = UCB.*prod(1-correlation,2);
end
% the objective needs to be maximized
obj = UCB;


end





