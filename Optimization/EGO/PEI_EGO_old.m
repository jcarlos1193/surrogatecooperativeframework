function [sample_x,sample_y,f_min]=PEI_EGO(fun_name,sample_x,sample_y,max_evaluation,num_q,optimum,num_vari,design_space,opt)
num_initial=0; % ric
%figure(10)
% record the f_min in each iteration
f_min = zeros(ceil((max_evaluation-num_initial)/num_q)+1,1);
% the current best solution
f_min(1) = min(sample_y);
% the current iteration and evaluation
%evaluation = size(sample_x,1);
evaluation=0;
max_evaluation=max_evaluation+size(sample_x,1);
iteration = 0;
% plot the iteration history
%plot(iteration,f_min(1),'r-o');title(sprintf('iteration: %d, evaluations:%d',iteration,evaluation));drawnow;
% print the current information to the screen
fprintf(' iter.: %d, eval.: %d, current best sol.: %f: optimum: %f\n', iteration, evaluation, f_min(1), optimum);
%--------------------------------------------------------------------------
% the iteration
while evaluation < max_evaluation 
    [sample_x,ia]=unique(sample_x,'row');
    sample_y=sample_y(ia);
    % build (or rebuild) the initial Kriging model
    kriging_model = dacefit(sample_x,sample_y,opt.pol,opt.ker,1*ones(1,num_vari),0.001*ones(1,num_vari),1000*ones(1,num_vari));
    % initialize the candidate points and other parameters
    %sigma2=kriging_model.sigma2;
    num_k = min(num_q,max_evaluation - evaluation);
    best_x = zeros(num_k,num_vari);
    point_added = [];
    % find the candidates based on pseudo EI criterion
    for ii = 1: num_k
        % the pseudo Expected Improvement criterion
        
        if strcmp(opt.Infill_Criterion,'EI')
        infill_criterion = @(x)infill_pseudo_EI(x, kriging_model, f_min(iteration+1), point_added);
        elseif strcmp(opt.Infill_Criterion,'UCB')
        infill_criterion = @(x)Infill_Pseudo_UCB(x, kriging_model, f_min(iteration+1), point_added,opt);
        end
        % find the point with the highest EI value using DE algorithm
        best_x(ii,:) = DE(infill_criterion, num_vari, design_space(1,:), design_space(2,:), 50, 200);
        % update point_added
        point_added = best_x(1:ii,:);
    end
    % evaluating the candidate with the real function
    best_y = feval(fun_name,best_x);
    % add the new point to design set
    sample_x = [sample_x;best_x];
    if size(best_y,2)>1
        best_y=best_y';
    end
    sample_y = [sample_y;best_y];
    [sample_x,ia]=unique(sample_x,'row');
    sample_y=sample_y(ia);
    % updating some parameters
    evaluation = size(sample_x,1);
    iteration = iteration + 1;
    f_min(iteration + 1) = min(sample_y);
    % plot the iteration history
    %plot(0:iteration,f_min(1:iteration+1),'r-o');title(sprintf('iteration: %d, evaluations:%d',iteration,evaluation));drawnow;
    % print the current information to the screen
    fprintf(' iter.: %d, eval.: %d, current best sol.: %f: optimum: %f\n', iteration, evaluation, f_min(iteration + 1), optimum);
end




