function [sample_x,sample_y,f_min]=PEI_EGO2(fun_name,sample_x,sample_y,max_evaluation,num_q,optimum,value_to_reach,num_vari,design_space,opt, name_file_results)
num_initial=0; % ric

% inicializacion de la matriz de informacion
% problem information
Data.name_file_results = check_number_of_filename(name_file_results);
Data.initial_points = sample_x;
Data.Problem=fun_name;
Data.MaxEvals = max_evaluation/num_q;
Data.SurrogateModel='kriging';
Data.SamplingTechnique=['PEI_', opt.Infill_Criterion];
Data.InitialDesign='SLHD';
Data.fbest = Inf;
Data.qpoints = num_q;
Data.S = sample_x;
Data.info_iter = {};

sample_y = [];

for i = 1 : size(sample_x,1)

   faux = feval(fun_name,sample_x(i,:));
   sample_y = [sample_y; faux];
   
   if faux < Data.fbest
        Data.fbest = faux;
        Data.xbest = sample_x(i, :);
   end

end

Data.Y = sample_y;

%figure(10)
% record the f_min in each iteration
f_min = zeros(ceil((max_evaluation-num_initial)/num_q)+1,1);
% the current best solution
f_min(1) = min(sample_y);
% the current iteration and evaluation
%evaluation = size(sample_x,1);
evaluation=0;
max_evaluation=max_evaluation+size(sample_x,1);
iteration = 0;
% plot the iteration history
%plot(iteration,f_min(1),'r-o');title(sprintf('iteration: %d, evaluations:%d',iteration,evaluation));drawnow;
% print the current information to the screen
fprintf(' iter.: %d, eval.: %d, current best sol.: %f: optimum: %f\n', iteration, evaluation, f_min(1), optimum);
%--------------------------------------------------------------------------
% the iteration
while evaluation < max_evaluation 
    [sample_x,ia]=unique(sample_x,'row');
    sample_y=sample_y(ia);
    % build (or rebuild) the initial Kriging model
    kriging_model = dacefit(sample_x,sample_y,opt.pol,opt.ker,1*ones(1,num_vari),0.001*ones(1,num_vari),1000*ones(1,num_vari));
    % initialize the candidate points and other parameters
    
    if strcmp(opt.Infill_Criterion,'FIM')
        out=InformationMatrix(sample_x,sample_y,kriging_model.theta,opt.ker,kriging_model.Ssc,opt.c);
    end
    %sigma2=kriging_model.sigma2;
    num_k = min(num_q,max_evaluation - evaluation);
    
    best_x = zeros(num_k,num_vari);
    point_added = [];
    % find the candidates based on pseudo EI criterion
    for ii = 1: num_k
        % the pseudo Expected Improvement criterion
        
        if strcmp(opt.Infill_Criterion,'EI')
            infill_criterion = @(x)infill_pseudo_EI(x, kriging_model, f_min(iteration+1), point_added);
        elseif strcmp(opt.Infill_Criterion,'UCB')
            infill_criterion = @(x)Infill_Pseudo_UCB(x, kriging_model, f_min(iteration+1), point_added,opt);
        elseif strcmp(opt.Infill_Criterion,'LCB')
            
            [sample_x_aux, ia] = unique([sample_x; point_added],'rows','stable');
            sample_y_aux = [sample_y; zeros(size(point_added,1),1)];
            sample_y_aux = sample_y_aux(ia);
            
            kriging_model_var = dacefit(sample_x_aux,sample_y_aux,opt.pol,opt.ker,1*ones(1,num_vari),0.001*ones(1,num_vari),1000*ones(1,num_vari));
            
            infill_criterion = @(x) Infill_Pseudo_LCB(x, kriging_model, kriging_model_var, opt);
            
        elseif strcmp(opt.Infill_Criterion,'FIM')
            infill_criterion = @(x)Infill_Pseudo_FIM(x, kriging_model,  point_added,opt,out);
        end
        
        if strcmp(opt.Infill_Criterion,'FIM')
            options = optimoptions('particleswarm','SwarmSize',100,'HybridFcn',@fmincon,'MaxStallIterations',500);
            best_x(ii,:) = particleswarm(infill_criterion,num_vari,design_space(1,:), design_space(2,:),options);
            
            %best_x(ii,:) = fminsearch(infill_criterion,best_x(end,:));
        else 
        % find the point with the highest EI value using DE algorithm
        %%%%%%%%%best_x(ii,:) = DE(infill_criterion, num_vari, design_space(1,:), design_space(2,:), 50, 200);
            options_pso = optimoptions('particleswarm','Display','off','MaxIterations',100,'SwarmSize',50,'MaxStallIterations',100, 'UseVectorized', true);
            best_x(ii,:) = particleswarm(infill_criterion, num_vari, design_space(1,:), design_space(2,:), options_pso);
        end
        % update point_added
        point_added = best_x(1:ii,:);
    end
    % evaluating the candidate with the real function
    best_y = [];
    
    for i = 1 : size(best_x,1)
        best_y = [best_y, feval(fun_name, best_x(i,:))];
    end
    
    % add the new point to design set
    sample_x = [sample_x;best_x];
    Data.S = [Data.S; best_x];
    
    if size(best_y,2)>1
        best_y=best_y';
    end
    
    sample_y = [sample_y;best_y];
    Data.Y = [Data.Y; best_y];
    
    [sample_x,ia]=unique(sample_x,'row');
    sample_y=sample_y(ia);
    
    [F_sel_best, idx_best] = min(best_y); % get the best from m points
    
    if (F_sel_best < Data.fbest) %new point is better than best point found so far?
        Data.xbest = best_x(idx_best,:); %update best point found so far
        Data.fbest = F_sel_best; %update best function value found so far
        Data.iterbest = iteration+1;
    end
    
    Data.info_iter{iteration+1, 1} = best_x;
    Data.info_iter{iteration+1, 2} = best_y;
    Data.info_iter{iteration+1, 3} = best_x(idx_best,:);
    Data.info_iter{iteration+1, 4} = F_sel_best;
    
    save(Data.name_file_results,'Data');
    fprintf(' iter.: %d, eval.: %d, current best sol.: %f: optimum: %f\n', iteration+1, evaluation, f_min(iteration + 1), optimum);
    
     %% Stop algorithm if a value is met
    if exist('value_to_reach', 'var')
        
        if value_to_reach >= F_sel_best
            fprintf('Break - Number of function evaluation: %4.0f; Best feasible function value: %f\n', iteration+1,Data.fbest)
            break;
        end
        
    end
    
    %% updating some parameters
    evaluation = size(sample_x,1);
    iteration = iteration + 1;
    f_min(iteration + 1) = min(sample_y);
    
    % plot the iteration history
    %plot(0:iteration,f_min(1:iteration+1),'r-o');title(sprintf('iteration: %d, evaluations:%d',iteration,evaluation));drawnow;
    % print the current information to the screen
    
end




