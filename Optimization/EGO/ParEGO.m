function [f_min, x_best, data_info] = ParEGO(fun, bounds, max_iters, initial_design, num_initial_sample, qpoints, value_to_reach, name_file_results)

% bounds
xlow = bounds(:,1)';
xup = bounds(:,2)';
% number of dimensions
num_vari = length(xlow);
% Info
data_info = struct;

% Set the number of Res_EGO file
str_file = check_number_of_filename(name_file_results);

% this optimizer us the particle swarm optimization implemented in MATLAB 2016b
options = optimoptions('particleswarm','SwarmSize',100,'MaxIterations',100,'MaxStallIterations',100,'Display','off', 'UseVectorized', true);

% the 0th iteration
% initial design points using Latin hypercube sampling method
feval_start = tic;
[sample_x, sample_y] = InitialDesignPoints(initial_design, num_initial_sample, xlow, xup, fun);
data_info.feval_time_it0 = toc(feval_start);

% the current best solution
[f_min, ix_best] = min(sample_y);
x_best = sample_x(ix_best,:);

% save the information into a struct var and file
data_info.X_it0 = sample_x;
data_info.Y_it0 = sample_y;
data_info.f_best = f_min;
data_info.x_best = x_best;

% the current iteration and evaluation
iteration = 0;
evaluation = size(sample_x,1);

% print the current information to the screen
fprintf(' iteration: %d, evaluation: %d, current best solution: %f\n', iteration, evaluation, f_min);
iteration = 1;

%--------------------------------------------------------------------------
% the iteration
while iteration <= max_iters
    
    time_start = tic;
    
    % build (or rebuild) the initial Kriging model
    kriging_model = dacefit(sample_x, sample_y, 'regpoly0', 'corrgauss', 1*ones(1,num_vari), 0.001*ones(1,num_vari), 1000*ones(1,num_vari));
    
    % initial the candidate points and other parameters
    best_x = zeros(qpoints, num_vari);
    point_added = [];
    
    %--------------------------------------------------------------------------
    % find the candidates based on pseudo EI criterion
    for ii = 1 : qpoints
        % the pseudo Expected Improvement criterion
        infill_criterion = @(x)infill_pseudo_EI(x, kriging_model, f_min, point_added);
        % find the point with the highest EI value using ga algorithm
        best_x(ii,:)= particleswarm(infill_criterion, num_vari, xlow, xup, options);
        % update point_added
        point_added = best_x(1:ii,:);
    end
    
    % evaluating the candidate with the real function. IT CAN BE PARALLELIZED
    % best_y = feval(fun,best_x);
    feval_start = tic;
    
    best_y = zeros(qpoints, 1);
    for idx = 1 : qpoints
        best_y(idx,1) = feval(fun, best_x(idx,:));
    end
    
    data_info.feval_time(iteration) = toc(feval_start);
    
    %--------------------------------------------------------------------------
    % add points and rebuild Kriging model
    % add the new point to design set
    sample_x = [sample_x; best_x];
    sample_y = [sample_y; best_y];
    
    % updating some parameters
    evaluation = size(sample_x,1);
    [f_min, ix_best] = min(sample_y);
    x_best = sample_x(ix_best,:);
    
    % save the information into a struct var and file
    data_info.X{iteration} = best_x;
    data_info.Y{iteration} = best_y;
    data_info.f_best = f_min;
    data_info.x_best = x_best;
    data_info.num_evaluations = evaluation;
    data_info.num_iterations = iteration; % without considering the 0th iteration
    data_info.time_iter(iteration) = toc(time_start);
    
    save(str_file, 'data_info');
    
    % print the current information to the screen
    fprintf(' iteration: %d, evaluation: %d, current best solution: %f\n', iteration, evaluation, f_min);
    
    % Stop algorithm if a value is met
    if exist('value_to_reach', 'var')
        
        if value_to_reach >= f_min
            break;
        end
        
    end
    
    iteration = iteration + 1;
    
end


end

