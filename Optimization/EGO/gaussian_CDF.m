function y = gaussian_CDF(x)
y=0.5*(1+erf(x/sqrt(2)));
end