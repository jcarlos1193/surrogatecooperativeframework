function stop = stop_value_to_reach(x, optimValues, state)

global value_to_reach;
stop = false;

fval = optimValues.fval;

if exist('value_to_reach', 'var')
    if value_to_reach >= fval
        stop = true;
    end
end

end
