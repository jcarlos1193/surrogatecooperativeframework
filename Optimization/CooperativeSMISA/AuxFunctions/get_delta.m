function epsilon = get_delta(num, LB, UB, Z_i) % beta

points = LB+rand(num,length(LB)).*(UB-LB);

% epsilon = quantile(min(pdist2(Z_i, points)), beta);
epsilon = max(min(pdist2(Z_i, points)));

end

