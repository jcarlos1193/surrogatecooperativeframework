function d = distancia(Z_i, y_i, options, type)

if strcmp(type, 'euclidean')

    d = min(pdist2(Z_i, y_i));
    
elseif strcmp(type, 'correlation')

    d = dist_correlation(Z_i, y_i, options.kriging_model);
    
end


end

