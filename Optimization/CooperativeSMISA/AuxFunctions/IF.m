function dist = IF(X)

%--------------------------------------------------------------------------
% if this is not the first infill point
if ~isempty(point_added)
    % the scaling of x
    scaled_x = (x - kriging_model.Ssc(1,:)) ./ kriging_model.Ssc(2,:);
    scaled_point_added = (point_added - kriging_model.Ssc(1,:)) ./ kriging_model.Ssc(2,:);
    correlation = zeros(size(scaled_x,1),size(scaled_point_added,1));
    for ii =1:size(scaled_point_added,1)
        dx = scaled_x - scaled_point_added(ii,:);
        correlation(:,ii) = feval(kriging_model.corr, kriging_model.theta, dx);
    end
    % the Pseudo EI matrix
    EI = EI.*prod(1-correlation,2);
end

end