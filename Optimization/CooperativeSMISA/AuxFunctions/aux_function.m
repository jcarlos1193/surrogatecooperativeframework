function z = aux_function(Z_i, x, Sx, epsilon)

c = epsilon - distancia(Z_i, x, nan, 'euclidean');

if c > 0
    z = Inf;
    return;
end

z = Sx(x);


end

