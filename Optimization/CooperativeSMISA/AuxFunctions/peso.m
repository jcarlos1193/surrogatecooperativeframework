function w_i = peso(y_i, Sx, options)

    if strcmp(options.type, 'localqual')

        fvalue = Sx(y_i);

        if options.Ymax - options.epsilon <= fvalue
            w_i = log(1 - options.epsilon / (options.Ymax - options.Ymin));
        elseif options.Ymin + options.epsilon <= fvalue && fvalue < options.Ymax - options.epsilon
            w_i = log((fvalue - options.Ymin) / (options.Ymax - options.Ymin));
        else
            w_i = log(options.epsilon / (options.Ymax - options.Ymin)) - (1 / options.epsilon) * (options.Ymin + options.epsilon - fvalue);
        end
        
    elseif strcmp(options.type, 'CORS')
        
        w_i = Sx(y_i);
        
    elseif strcmp(options.type, 'localQual_v2')
        
        fvalue = rescale(Sx(y_i), 'InputMin', options.Ymin, 'InputMax', options.Ymax);
        
        if fvalue > options.epsilon
            w_i = log(options.alpha * fvalue);
        else
            w_i = log(options.alpha * options.epsilon) - 1 + (1 / options.epsilon) * fvalue;
        end
        
    elseif strcmp(options.type, 'EI')
        
        [y, mse] = predictor(y_i, options.kriging_model);
        s = sqrt(max(0, mse));
        
        % calcuate the EI value
        EI = (options.Ymin - y).*gaussian_CDF((options.Ymin-y)./s)+s.*gaussian_PDF((options.Ymin-y)./s);
        
        w_i = -EI;
        
    end
    
end

