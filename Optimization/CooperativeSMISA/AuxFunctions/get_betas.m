function betas = get_betas(betas, qpoints)

betas = sort(betas, 'descend');

if qpoints <= length(betas)
    return;
end

betas_old = betas;
betas_old(end) = [];

idx_min = length(betas_old);
idx_max = 1;
is_min = 1;

for idx = 1 : qpoints - length(betas)
   
    if idx_min < idx_max
        idx_min = length(betas_old);
        idx_max = 1;
        is_min = 1;
    end
    
    if is_min
        idx_point = idx_min;
        idx_min = idx_min - 1;
        is_min = 0;
    else
        idx_point = idx_max;
        idx_max = idx_max + 1;
        is_min = 1;
    end
    
    betas = [betas, betas_old(idx_point)];

end

betas = sort(betas, 'descend');

end