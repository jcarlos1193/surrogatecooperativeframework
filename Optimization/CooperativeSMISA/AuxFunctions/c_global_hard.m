function [c, ceq] = c_global_hard(l_epsilon, X, Y)

c = zeros(1, size(Y, 1));

for idx_y = 1 : size(Y, 1)
    
    Z_i = puntos(X, Y, idx_y, 'cors_full');
 
    c(idx_y) = l_epsilon(idx_y) - distancia(Z_i, Y(idx_y, :), nan, 'euclidean');

end

ceq = [];

end

