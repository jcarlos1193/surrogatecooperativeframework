function Y = global_hard( x_n, Sx, data_model, dim )

x_n = vec2mat(x_n, dim);

%% Calculo del valor del modelo de calidad

options.Ymax = data_model.Ymax;
options.Ymin = data_model.Ymin;
options.epsilon = data_model.alfa;
options.type = data_model.type;
options.kriging_model = data_model.kriging_model;

Y = 0;

for idx = 1 : size(x_n,1)
   
    wx = peso(x_n(idx, :), Sx, options);
    Y = Y + wx;

end

end

