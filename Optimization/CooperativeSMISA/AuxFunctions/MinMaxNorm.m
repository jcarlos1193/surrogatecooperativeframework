function norm_X = MinMaxNorm( X )

    norm_X = ones(size(X,1),size(X,2));

    for idx = 1 : size(X, 2)
        
        aux_x = X(:,idx);
        aux_bounds = max(aux_x) - min(aux_x);
        
        if aux_bounds == 0
            norm_X(:,idx) = 0; 
            continue; 
        end
        
        norm_X(:,idx) = ( aux_x - min(aux_x) ) / aux_bounds;
        
    end


end

