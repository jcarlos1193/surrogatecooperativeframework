function d = dist_correlation(Z_i, y_i, kriging_model)

number_points = size(Z_i, 1);
correlation = zeros(number_points, 1);

scaled_y_i = (y_i - kriging_model.Ssc(1,:)) ./ kriging_model.Ssc(2,:);


for idx = 1 : number_points
    
    scaled_x = (Z_i(idx, :) - kriging_model.Ssc(1,:)) ./ kriging_model.Ssc(2,:);
    
    dx = scaled_x - scaled_y_i;
    correlation(idx) = 1 - feval(kriging_model.corr, kriging_model.theta, dx);
    
end
    
d = prod(correlation); %^ (1 / length(correlation));  

end