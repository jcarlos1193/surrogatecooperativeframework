function Z_i = puntos(X, Y, i, type)

if strcmp(type, 'full')

    Y(i, :) = [];
    Z_i = [X; Y];
    
elseif strcmp(type, 'full_heur')

    Z_i = [X; Y];
    
elseif strcmp(type, 'new_full')
    
    Y(i, :) = [];
    Z_i = Y;

elseif strcmp(type, 'sequential')
    
    Z_i = Y(1:(i-1), :);
    
elseif strcmp(type, 'cors_full')
    
    Z_i = [X; Y(1:(i-1), :)];
    
end


end

