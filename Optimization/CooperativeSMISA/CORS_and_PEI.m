function Data = CORS_and_PEI( Data, maxeval, Surrogate, lambda, gamma, dmodel, mmodel, beta, w_m )

qpoints = Data.qpoints;
value_to_reach = Data.value_to_reach;

Data.time_iter = [];
Data.info_iter = {};
num_main_iters = 1;

betas = [0.9 0.75 0.25 0.05 0.03 0];
betas = get_betas(betas, qpoints);

num_no_improvements = 0;
conseq_no_improvements = Inf;

pruning_value = Inf; % Inf indicates no pruning

function [c, ceq] = aux_function(c)
    ceq = [];
end

idx_global_betas = 1;
Data.S_old = Data.S;
previous_bestf = Data.fbest;


%% Main iterations
while num_main_iters <= maxeval 

    t_start = tic;
    
    %% set options and subrrogate model to create patriksson quality model
    [lambda, gamma, dmodel, mmodel, beta, w_m] = FitSurrogateModel(Data, Surrogate); 
    Sx = @(x) PredictFunctionValues(Data, Surrogate, x, lambda, gamma, dmodel, beta, mmodel, w_m);  

    kriging_model = dacefit(Data.S,Data.Y,'regpoly0','corrgauss',1*ones(1,Data.dim),0.001*ones(1,Data.dim),1000*ones(1,Data.dim));
    
    options.Ymin = min(Data.Y);
    options.kriging_model = kriging_model;
    
    opt.kriging_model = kriging_model;
    
    opt_fmincon = optimoptions('fmincon', 'Display', 'off');
    opt_pso = optimoptions('particleswarm','Display','off','MaxIterations',100,'SwarmSize',50,'MaxStallIterations',100);
    
    %% Optimization
    xselected = [];
    delta = get_delta(500, Data.xlow, Data.xup, Data.S_old);
    
    for idx_y = 1 : 2 : qpoints

        %% CORS
        Z_i = puntos(Data.S_old, xselected, idx_y, 'cors_full');
        options.type = 'CORS';
        qual_func = @(x) peso(x, Sx, options);
        epsilon = delta * betas( mod(idx_global_betas - 1, length(betas)) + 1 );

        c = @(x) aux_function(epsilon - distancia(Z_i, x, nan, 'euclidean'));

        x_cors = fmincon(qual_func, Data.xlow + rand(1, Data.dim) .* (Data.xup - Data.xlow), [], [] , [], [], Data.xlow, Data.xup, c, opt_fmincon);
        
        %% PEI
        Z_i = puntos([], xselected, idx_y, 'sequential');
        options.type = 'EI';
        qual_func = @(x) peso(x, nan, options) * distancia(Z_i, x, opt, 'correlation');
       
        x_pei = particleswarm(qual_func, Data.dim, Data.xlow, Data.xup, opt_pso);
        
        xselected = [xselected; x_cors; x_pei];
        idx_global_betas = idx_global_betas + 1;
       
    end
    
    Data.S = [Data.S; xselected]; %sample site matrix
    Data.S_old = [Data.S_old; xselected];
    
    %% perform function evaluation at the selected point    
    fevalst = tic;
    Fselected = [];
    
    for i = 1 : size(xselected,1)
        Fselected = [Fselected; feval(Data.objfunction,xselected(i, :))];
    end
    
    Data.fevaltime = [Data.fevaltime;toc(fevalst)]; %record objective function evaluation time

    %% update data vectors
    [F_sel_best, idx_best] = min(Fselected); % get the best from m points
    
    if (F_sel_best < Data.fbest) %new point is better than best point found so far?
        Data.xbest = xselected(idx_best,:); %update best point found so far
        Data.fbest = F_sel_best; %update best function value found so far
        Data.iterbest = num_main_iters;
    end

    Data.Y = [Data.Y; Fselected]; %objective function values
    
    %% reduce number of points
    if size(Data.S, 1) >= pruning_value
        [X_aux, ~] = reduce_points(qpoints, Data.S, Data.Y);

        [~, ix_X1, ~] = intersect(Data.S, X_aux, 'rows');
        ix_X1 = sort(ix_X1);
        
        Data.S = X_aux;
        Data.Y = Data.Y(ix_X1, :);
        disp('Aplicando poda');
    end
    
    %% update data vectors
    Data.Ymed = Data.Y; %data where large function values set to median, for calculation of surrogate model parameters
    MedY = median(Data.Y);
    Data.Ymed(Data.Y>MedY) = MedY;
    
    %% save information
    Data.info_iter{num_main_iters, 1} = xselected;
    Data.info_iter{num_main_iters, 2} = Fselected;
    Data.info_iter{num_main_iters, 3} = xselected(idx_best,:);
    Data.info_iter{num_main_iters, 4} = F_sel_best;
    Data.time_iter = [Data.time_iter; toc(t_start)];
    
    save(Data.name_file_results,'Data');
    
    fprintf('------------\n');
    fprintf('Number of function evaluation: %4.0f; Best feasible function value: %f\n', num_main_iters,Data.fbest);
    fprintf('Function value: %f\n', F_sel_best);
    fprintf('Time Optimization: %f\n', Data.time_iter(num_main_iters));
    fprintf('------------\n');
    
    %% Stop algorithm if a value is met
    if exist('value_to_reach', 'var')
        
        if value_to_reach >= F_sel_best
            fprintf('Break - Number of function evaluation: %4.0f; Best feasible function value: %f\n', num_main_iters,Data.fbest)
            break;
        end
        
    end

    num_main_iters = num_main_iters + 1;
    
    %% Restart algorithm with a new set of initial points
    if num_no_improvements == conseq_no_improvements
        
        Data.initial_points = StartingDesign(Data.InitialDesign, Data.NumberStartPoints, Data);
        Data.S_old = [Data.S_old; Data.initial_points];
        Data.S = Data.initial_points;
        
        fevalst = tic;
        Fvalues = [];
    
        for i = 1 : size(Data.S,1)
            Fvalues = [Fvalues; feval(Data.objfunction, Data.S(i, :))];
        end
        
        Data.fevaltime = [Data.fevaltime; toc(fevalst)];
        
        Data.Y = Fvalues;
        
        Data.Ymed = Data.Y; 
        MedY = median(Data.Y);
        Data.Ymed(Data.Y>MedY) = MedY;
        
        num_no_improvements = 0;
        
        disp('Aplicando reinicializacion');
        
    else
        
        previous_fbest_aux = previous_bestf;
        
        if previous_fbest_aux == 0
            previous_fbest_aux = 0.01;
        end
        
        if abs(previous_fbest_aux * (0.1 / 100)) > abs(previous_bestf - F_sel_best) || previous_bestf < F_sel_best  
            num_no_improvements = num_no_improvements + 1;   
        end
        
    end
    
    previous_bestf = Data.fbest;
    
   
    %% create a graphic with all points including initial evaluations
    if (Data.dim == 2) && false

        figure(1)

        bounds = [Data.xlow; Data.xup]';

        if isfield(Data,'precision') == 1
            precision = Data.precision;
        else
            precision = 0.1;
        end
        %%%%%%%%%%%%%%%%%%%%%55
        func = Data.objfunction;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fun_with_variables = @(varargin) func(cell2mat(varargin));
        [F, plot_x] = getFunctionValues( fun_with_variables, bounds, precision );

        [C, h] = contour(plot_x{2}, plot_x{1}, F, 25);

        hold on

        %axis square
        %clabel(C,h); % para indicar los valores de la funcion sobre la curva
        title('Curvas de nivel de la funcion objetivo', 'fontSize', 18);
        xlabel('X1', 'fontsize', 18);
        ylabel('X2', 'fontsize', 18);

        % print all points including initial and sampled points
        for idx = 1 : size(Data.initial_points, 1)
            plot(Data.initial_points(idx,1), Data.initial_points(idx,2), 'o', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r', 'MarkerSize', 5);
        end

        for idx = size(Data.initial_points,1) + 1 : size(Data.S, 1)
            plot(Data.S(idx,1), Data.S(idx,2), '*', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b', 'MarkerSize', 8);
        end
        hold off

    end
    
end

end

