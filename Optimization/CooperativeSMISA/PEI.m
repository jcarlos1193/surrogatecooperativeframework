function Data = PEI( Data, maxeval, Surrogate, lambda, gamma, dmodel, mmodel, beta, w_m )

qpoints = Data.qpoints;
value_to_reach = Data.value_to_reach;

Data.time_iter = [];
Data.info_iter = {};
num_main_iters = 1;

pruning_value = Inf; % Inf indicates no pruning

Data.S_old = Data.S;

%% Main iterations
while num_main_iters <= maxeval 

    t_start = tic;
    
    %% set options and subrrogate model to create patriksson quality model
    %[lambda, gamma, dmodel, mmodel, beta, w_m] = FitSurrogateModel(Data, Surrogate); 
    Sx = nan;%@(x) PredictFunctionValues(Data, Surrogate, x, lambda, gamma, dmodel, beta, mmodel, w_m);  
    kriging_model = dacefit(Data.S,Data.Y,'regpoly0','corrgauss',1*ones(1,Data.dim),0.001*ones(1,Data.dim),1000*ones(1,Data.dim));

    options.Ymax = max(Data.Y);
    options.Ymin = min(Data.Y);
    options.type = 'EI';
    options.kriging_model = kriging_model;
    
    opt.kriging_model = kriging_model;
    
    options_pso = optimoptions('particleswarm','Display','off','MaxIterations',100,'SwarmSize',100,'MaxStallIterations',100);

    xselected = [];
    
    for idx_y = 1 : qpoints

       Z_i = puntos([], xselected, idx_y, 'sequential');
       qual_func = @(x) peso(x, Sx, options) * distancia(Z_i, x, opt, 'correlation');
       
       global_sol = particleswarm(qual_func, Data.dim, Data.xlow, Data.xup, options_pso);
       xselected = [xselected; global_sol];
       
    end

    Data.S = [Data.S; xselected]; %sample site matrix
    Data.S_old = [Data.S_old; xselected];
    
    %% perform function evaluation at the selected point    
    fevalst=tic;
    Fselected = [];
    
    for i = 1 : size(xselected,1)
        Fselected = [Fselected; feval(Data.objfunction,xselected(i, :))];
    end
 
    Data.fevaltime = [Data.fevaltime;toc(fevalst)]; %record objective function evaluation time

    %% update data vectors
    [F_sel_best, idx_best] = min(Fselected); % get the best from m points
    
    if (F_sel_best < Data.fbest) %new point is better than best point found so far?
        Data.xbest = xselected(idx_best,:); %update best point found so far
        Data.fbest = F_sel_best; %update best function value found so far
        Data.iterbest = num_main_iters;
    end
    
    Data.Y = [Data.Y; Fselected]; %objective function values
    
    %% reduce number of points
    if size(Data.S, 1) >= pruning_value
        [X_aux, ~] = reduce_points(qpoints, Data.S, Data.Y);
        
        [~, ix_X1, ~] = intersect(Data.S, X_aux, 'rows');
        ix_X1 = sort(ix_X1);
        
        Data.S = X_aux;
        Data.Y = Data.Y(ix_X1, :);
        disp('Aplicando poda')
    end
    
    %% update data vectors
    Data.Ymed = Data.Y; %data where large function values set to median, for calculation of surrogate model parameters
    MedY = median(Data.Y);
    Data.Ymed(Data.Y>MedY) = MedY;
    
    %% save information
    Data.info_iter{num_main_iters, 1} = xselected;
    Data.info_iter{num_main_iters, 2} = Fselected;
    Data.info_iter{num_main_iters, 3} = xselected(idx_best,:);
    Data.info_iter{num_main_iters, 4} = F_sel_best;
    Data.time_iter = [Data.time_iter; toc(t_start)];
    
    save(Data.name_file_results,'Data');
    
    fprintf('------------\n');
    fprintf('Number of function evaluation: %4.0f; Best feasible function value: %f\n', num_main_iters,Data.fbest);
    fprintf('Function value: %f\n', F_sel_best);
    fprintf('Time Optimization: %f\n', Data.time_iter(num_main_iters));
    fprintf('------------\n');
    
    %% Stop algorithm if a value is met
    if exist('value_to_reach', 'var')
        
        if value_to_reach >= F_sel_best
            fprintf('Break - Number of function evaluation: %4.0f; Best feasible function value: %f\n', num_main_iters,Data.fbest)
            break;
        end
        
    end

    num_main_iters = num_main_iters + 1;
    
    
    %% create a graphic with all points including initial evaluations
    if (Data.dim == 2) && false

        figure(1)

        bounds = [Data.xlow; Data.xup]';

        if isfield(Data,'precision') == 1
            precision = Data.precision;
        else
            precision = 0.1;
        end

        qual_func = Data.objfunction;
        fun_with_variables = @(varargin) qual_func(cell2mat(varargin));
        [F, plot_x] = getFunctionValues( fun_with_variables, bounds, precision );

        [C, h] = contour(plot_x{2}, plot_x{1}, F, 25);

        hold on

        %axis square
        %clabel(C,h); % para indicar los valores de la funcion sobre la curva
        title('Curvas de nivel de la funcion objetivo', 'fontSize', 18);
        xlabel('X1', 'fontsize', 18);
        ylabel('X2', 'fontsize', 18);

        % print all points including initial and sampled points
        for idx = 1 : size(Data.initial_points, 1)
            plot(Data.initial_points(idx,1), Data.initial_points(idx,2), 'o', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r', 'MarkerSize', 5);
        end

        for idx = size(Data.initial_points,1) + 1 : size(Data.S, 1)
            plot(Data.S(idx,1), Data.S(idx,2), '*', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b', 'MarkerSize', 8);
        end
        hold off

    end
    
end

end

