function [F, x_all_dim] = getFunctionValues( objfunction, bounds, precision )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% objfunction - objetive function to be integrated - Ex: @(x,y,z) y*sin(x)+z*cos(x)
% bounds - a vector with min and max values for each dimension - Ex: [0,pi;0,1;-1,1]
% precision - distance between points to be valued - Ex: 0.1
% F - final values of objetive function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dim = size(bounds,1);
x_all_dim = cell(1,dim);

for idx = 1 : dim
    x_all_dim{idx} = (bounds(idx,1):precision:bounds(idx,2));
end

if dim > 1
    % swapping first and second elements of matrix
    aux_var = x_all_dim{1};
    x_all_dim{1} = x_all_dim{2};
    x_all_dim{2} = aux_var;
end

X_grid = cell(1,dim);
[X_grid{:}] = ndgrid(x_all_dim{:});

if dim > 1
    aux_var = X_grid{1};
    X_grid{1} = X_grid{2};
    X_grid{2} = aux_var;
end

F = arrayfun(objfunction, X_grid{:});

end

