function plot_sample(sol,X_sample,X,X1,Y1,Z)

n_K=size(X_sample,2);
for i=1:n_K
figure(i+1)
if size(sol,2)>1
plot(sol(:,1),sol(:,2),'gv',...
    'LineWidth',2,...
    'MarkerSize',10,...
    'MarkerEdgeColor','r',...
    'MarkerFaceColor',[0.75,0.75,0.75])
end
hold on
plot(X{i}(:,1),X{i}(:,2),'*','MarkerSize',5)
hold on

plot(X_sample{i}(:,1),X_sample{i}(:,2),'gv',...
    'LineWidth',2,...
    'MarkerSize',10,...
    'MarkerEdgeColor','b',...
    'MarkerFaceColor',[0.5,0.5,0.5])
if nargin>2
contour(X1, Y1, Z, 20);
legend('optimum','sampled data','surrogate-model data')
title(['Agente de busqueda ',num2str(i)],'FontSize',20)
colormap(hsv)
end
end