function str_file = check_number_of_filename(fname)

num_file = 1;
str_file = [fname, num2str(num_file), '.mat'];

while true
    if isfile(str_file)
        num_file = num_file + 1;
        str_file = [fname, num2str(num_file), '.mat'];
    else
        break;
    end
end

end

