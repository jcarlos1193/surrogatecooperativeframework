function points = generate_k_points(lb, ub, k)

rs = RandomStartPointSet('NumStartPoints', k);

problem = struct;
problem.x0 = randn(1, length(lb));
problem.lb = lb;
problem.ub = ub;

points = list(rs, problem);


end

