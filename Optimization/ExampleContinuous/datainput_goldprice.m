function Data = datainput_goldprice

Data.xlow =[-2,-2];  % variable lower bounds
Data.xup=[2,2];     % variable upper bounds
Data.dim=2;             %problem dimension
Data.integer=[];        %indices of variables with integer constraints
Data.continuous=(1:2);  %indices of continuous variables 
Data.objfunction=@(x)myfun(x); %handle to objective function
Data.known_best_f = 3;
Data.known_best_x = {[0, -1]};

end

%% Function definition

function y=myfun(xx) %objective function
x1 = xx(1);
x2 = xx(2);

fact1a = (x1 + x2 + 1)^2;
fact1b = 19 - 14*x1 + 3*x1^2 - 14*x2 + 6*x1*x2 + 3*x2^2;
fact1 = 1 + fact1a*fact1b;

fact2a = (2*x1 - 3*x2)^2;
fact2b = 18 - 32*x1 + 12*x1^2 + 48*x2 - 36*x1*x2 + 27*x2^2;
fact2 = 30 + fact2a*fact2b;

y = fact1*fact2;

end %myfun
