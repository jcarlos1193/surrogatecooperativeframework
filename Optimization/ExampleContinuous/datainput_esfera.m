function Data = datainput_esfera

Data.dim = 30; %problem dimension

Data.xlow = -5.12 * ones(1,Data.dim); %variable lower bounds
Data.xup = 5.12 * ones(1,Data.dim); %variable upper bounds

Data.objfunction= @(x) sum(x.^2);

Data.known_best_f = 0;
Data.stop_fvalue = (Data.objfunction(Data.xlow) - Data.known_best_f) * (0.01 / 100) + Data.known_best_f; % es 0 pero se corrige para tener en cuenta el error relativo
Data.known_best_x = zeros(1,Data.dim);

%objective function
Data.integer=[]; %indices of integer variables
Data.continuous=(1:Data.dim); %indices of continuous variables

end %function