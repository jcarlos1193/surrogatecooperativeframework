//
// This program is a full demonstration of a client program
// for participation in the black box optimization competition.
// It implements the Nelder Mead algorithm, see
// http://en.wikipedia.org/wiki/Nelder%E2%80%93Mead_method
//
// The algorithm is applied to all problems in a given track;
// it is demonstrated with the demo account on the trial track.
// The program shows one possible way to make use of the provided
// recovery mechanisms. In case of a crash or a power outage the
// program can be restarted and it will continue the optimization
// from the saved state. It handles network problems gracefully.
//


import java.io.*;
import java.net.*;
import java.util.*;


public class Client
{
	// configuration
	public static final String LOGFILEPATH = "logs/";
	public static final String USERNAME = "demoaccount";
	public static final String PASSWORD = "demopassword";
	public static final String TRACKNAME = "trial";
	public static final int LOGIN_DELAY_SECONDS  = 10;  // 10 seconds
	public static final int LOCK_DELAY_SECONDS   = 60;  // 60 seconds
	public static BBComp bbcomp;
	
	public static void main(String args[])
	{
		try
		{
			setupBBComp();
			safeLogin();
			safeSetTrack();
			int n = safeGetNumberOfProblems();

			// solve all problems in the track
			for (int i=0; i<n; i++) solveProblem(i);
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		finally
		{
		}
	}
	
	private static void solveProblem(int problemID)
	{
		// set the problem
		safeSetProblem(problemID);

		// obtain problem properties
		int bud = safeGetBudget(problemID);
		int dim = safeGetDimension(problemID);
		int evals = safeGetEvaluations(problemID);

		// output status
		if (evals == bud)
		{
			System.out.format("problem %d: already solved%n", problemID);
			return;
		}
		else if (evals == 0)
		{
			System.out.format("problem %d: starting from scratch%n", problemID);
		}
		else
		{
			System.out.format("problem %d: starting from evaluation %d%n", problemID, evals);
		}

		// reserve memory for the simplex and for a number of intermediate points
		List<PointValuePair> simplex = new ArrayList<PointValuePair>();
		for (int j=0; j<=dim; j++)
		{
			PointValuePair pair = new PointValuePair(dim);
			for (int i=0; i<dim; i++) 	
				pair.point.setElementAt((i == j) ? 0.8 : 0.2, i);
			simplex.add(pair);
		}
		int simplex_evaluated = 0;

		// recover algorithm state from saved evaluations
		for (int e=0; e<evals; e++)
		{
			try	{
			BBComp.HistoryReturn hr = bbcomp.history(e, dim);
			
			if (! hr.result)
			{
				// note: this evaluation is lost
				System.out.println("history() failed: " + bbcomp.errorMessage());
			}
			else
			{
				if (simplex_evaluated <= dim)
				{
					simplex.get(simplex_evaluated).point = hr.point;
					//simplex.elementAt(simplex_evaluated).point = hr.point;
					simplex.get(simplex_evaluated).value = hr.value;
					simplex_evaluated++;
				}
				else
				{
					Collections.sort(simplex);
					PointValuePair worst = simplex.get(dim);
					if (hr.value < worst.value) { worst.point = hr.point; worst.value = hr.value; }
				}
			}
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	}
		}

		// optimization loop
		while (safeGetEvaluations(problemID) < bud)
		{
			if (simplex_evaluated <= dim)
			{
				// evaluate the initial simplex
				simplex.get(simplex_evaluated).value = safeEvaluate(problemID, simplex.get(simplex_evaluated).point);
				simplex_evaluated++;
			}
			else
			{
				// step of the simplex algorithm
				Collections.sort(simplex);
				PointValuePair best = simplex.get(0);
				PointValuePair worst = simplex.get(dim);
				
				// compute centroid
				Vector<Double> x0 = simplex.get(0).point;
				for (int j=1; j<dim; j++) x0 = add(x0, simplex.get(j).point);
				x0 = scale(x0, 1.0 / (double)dim);

				// reflection
				PointValuePair xr = new PointValuePair(dim);
				xr.point = truncate2bounds(add(scale(x0, 2.0), scale(worst.point, -1.0)));
				xr.value = safeEvaluate(problemID, xr.point);
				if (best.value <= xr.value && xr.value < worst.value)
				{
					// replace worst point with reflected point
					simplex.set(dim, xr);
				}
				else if (xr.value < best.value)
				{
					if (safeGetEvaluations(problemID) >= bud) break;

					// expansion
					PointValuePair xe = new PointValuePair(dim);
					xe.point = truncate2bounds(add(scale(x0, 3.0), scale(worst.point, 2.0)));
					xe.value = safeEvaluate(problemID, xe.point);
					if (xe.value < xr.value)
					{
						// replace worst point with expanded point
						simplex.set(dim, xe);
					}
					else
					{
						// replace worst point with reflected point
						simplex.set(dim, xr);
					}
				}
				else
				{
					if (safeGetEvaluations(problemID) >= bud) break;

					// contraction
					PointValuePair xc = new PointValuePair(dim);
					xc.point = truncate2bounds(add(scale(x0, 0.5), scale(worst.point, 0.5)));
					xc.value = safeEvaluate(problemID, xc.point);
					if (xc.value < worst.value)
					{
						// replace worst point with contracted point
						simplex.set(dim, xc);
					}
					else
					{
						// reduction
						for (int j=1; j<=dim; j++)
						{
							if (safeGetEvaluations(problemID) >= bud) break;
							simplex.get(j).point = truncate2bounds(add(scale(best.point, 0.5), scale(simplex.get(j).point, 0.5)));
							simplex.get(j).value = safeEvaluate(problemID, simplex.get(j).point);
						}
					}
				}
			}
		}
	}

	public static class PointValuePair implements Comparable<PointValuePair>
	{
		public PointValuePair(int dim)
		{
			point = new Vector<Double>(dim);
			point.setSize(dim);
		}
		
		@Override
	    public int compareTo(PointValuePair o1) {
			if (this.value > o1.value)	return 1;
			if (this.value < o1.value)	return -1;
	        return 0;
	    }

		public Vector<Double> point;
		public double value;
	}
	

	private static Vector<Double> add(Vector<Double> x, Vector<Double> y)
	{
		Vector<Double> ret = new Vector<Double>();
		for (int i=0; i<x.size(); i++) ret.add(x.elementAt(i) + y.elementAt(i));
		return ret;
	}

	private static Vector<Double> scale(Vector<Double> x, double s)
	{
		Vector<Double> ret = new Vector<Double>();
		for (int i=0; i<x.size(); i++) ret.add(x.elementAt(i) * s);
		return ret;
	}

	private static Vector<Double> truncate2bounds(Vector<Double> x)
	{
		Vector<Double> ret = new Vector<Double>();
		for (int i=0; i<x.size(); i++)
		{
			double v = x.elementAt(i);
			if (v < 0.0) v = 0.0;
			else if (v > 1.0) v = 1.0;
			ret.add(v);
		}
		return ret;
	}

	private static void setupBBComp()
	{
		while (true)
		{
			try
			{
				bbcomp = null;
				// connect to black box library
				bbcomp = new BBComp();
							
				// setup
				if (! bbcomp.configure(true, LOGFILEPATH))
				{
					System.out.println("configure() failed: " + bbcomp.errorMessage());
					System.exit(1);
				}
				return;
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}
	}
	
	private static void safeLogin()
	{
		while (true)
		{
			String msg = new String();
			try	{
				boolean result = bbcomp.login(USERNAME, PASSWORD);
				if (result) return;
				msg = bbcomp.errorMessage();
				System.out.format("WARNING: login failed: %s%n", msg);	
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			if (msg.equals("already logged in")) return;
			try 							{	Thread.sleep(LOGIN_DELAY_SECONDS*1000);	} 
			catch(InterruptedException ex) 	{ 	Thread.currentThread().interrupt();		}
		}
	}

	private static void safeSetTrack()
	{
		while (true)
		{
			try	{
				boolean result = bbcomp.setTrack(TRACKNAME);
				if (result) return;
				System.out.format("WARNING: setTrack failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeLogin();
		}
	}

	private static int safeGetNumberOfProblems()
	{
		while (true)
		{
			try	{
				int result = bbcomp.numberOfProblems();
				if (result != 0) return result;
				System.out.format("WARNING: numberOfProblems failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeSetTrack();			
		}
	}

	private static void safeSetProblem(int problemID)
	{
		while (true)
		{
			String msg = new String();
			try	{
				boolean result = bbcomp.setProblem(problemID);
				if (result) return;
				msg = bbcomp.errorMessage();
				System.out.format("WARNING: setProblem failed: %s%n", msg);
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			if (msg.length() >= 22 && msg.substring(0, 22).equals("failed to acquire lock")) {	
				try 							{	Thread.sleep(LOCK_DELAY_SECONDS*1000);	} 
				catch(InterruptedException ex) 	{ 	Thread.currentThread().interrupt();		}}
			else safeSetTrack();
			
		}
	}

	private static double safeEvaluate(int problemID, Vector<Double> point)
	{
		while (true)
		{
			try	{
				BBComp.EvaluateReturn result = bbcomp.evaluate(point);
				if (result.result) return result.value;				
				System.out.format("WARNING: evaluate failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeSetProblem(problemID);
		}
	}

	private static int safeGetDimension(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.dimension();
				if (result != 0) return result;
				System.out.format("WARNING: dimension failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeSetProblem(problemID);			
		}
	}

	private static int safeGetBudget(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.budget();
				if (result != 0) return result;
				System.out.format("WARNING: budget failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeSetProblem(problemID);			
		}
	}

	private static int safeGetEvaluations(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.evaluations();
				if (result >= 0) return result;
				System.out.format("WARNING: evaluations failed: %s%n", bbcomp.errorMessage());
			}catch (Exception ex)			{	System.out.println(ex.getMessage());	setupBBComp();	}
			safeSetProblem(problemID);			
		}
	}
}  