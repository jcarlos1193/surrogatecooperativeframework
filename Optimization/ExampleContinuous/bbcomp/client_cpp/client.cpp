
// black box library header and function (pointer) declarations
#include <bbcomplib.h>
DLL_DECLARATIONS

// additional headers
#include <vector>
#include <iostream>
#include <stdexcept>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>


using namespace std;


// configuration
#define LOGFILEPATH "logs/"
#define USERNAME "demoaccount"
#define PASSWORD "demopassword"
#define TRACKNAME "trial"               // single-objective trail track
//#define TRACKNAME "trialMO"             // multi-objective trail track
#define LOGIN_DELAY_USECONDS  10000000  // 10 seconds
#define LOCK_DELAY_USECONDS   60000000  // 60 seconds


///////////////////////////////////////////////////////////
// auxiliary functions
//

#ifndef _WIN32
#include <unistd.h>
#endif

// sleep in microseconds
void u_sleep(long usec)
{
#ifdef _WIN32
	HANDLE timer;
	LARGE_INTEGER ft;
	ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time
	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);
#else
	usleep(usec);
#endif
}


///////////////////////////////////////////////////////////
// network failure resilient functions
//

void safeLogin()
{
	while (1)
	{
		int result = login(USERNAME, PASSWORD);
		if (result != 0) return;
		const char* msg = errorMessage();
		printf("WARNING: login failed: %s\n", msg);
		u_sleep(LOGIN_DELAY_USECONDS);
		if (strcmp(msg, "already logged in") == 0) return;
	}
}

void safeSetTrack()
{
	while (1)
	{
		int result = setTrack(TRACKNAME);
		if (result != 0) return;
		printf("WARNING: setTrack failed: %s\n", errorMessage());
		safeLogin();
	}
}

int safeGetNumberOfProblems()
{
	while (1)
	{
		int result = numberOfProblems();
		if (result != 0) return result;
		printf("WARNING: numberOfProblems failed: %s\n", errorMessage());
		safeSetTrack();
	}
}

void safeSetProblem(int problemID)
{
	while (1)
	{
		int result = setProblem(problemID);
		if (result != 0) return;
		const char* msg = errorMessage();
		printf("WARNING: setProblem failed: %s\n", msg);
		if (memcmp(msg, "failed to acquire lock", 22) == 0) u_sleep(LOCK_DELAY_USECONDS);
		else safeSetTrack();
	}
}

void safeEvaluate(int problemID, double* point, double* value)
{
	while (1)
	{
		int result = evaluate(point, value);
		if (result != 0) return;
		printf("WARNING: evaluate failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetDimension(int problemID)
{
	while (1)
	{
		int result = dimension();
		if (result != 0) return result;
		printf("WARNING: dimension failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetNumberOfObjectives(int problemID)
{
	while (1)
	{
		int result = numberOfObjectives();
		if (result != 0) return result;
		printf("WARNING: numberOfObjectives failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetBudget(int problemID)
{
	while (1)
	{
		int result = budget();
		if (result != 0) return result;
		printf("WARNING: budget failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetEvaluations(int problemID)
{
	while (1)
	{
		int result = evaluations();
		if (result >= 0) return result;
		printf("WARNING: evaluations failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}


// This example client program implements random search
// to optimize the first problem in the "trial" track.
void optimize()
{
	cout << "-------------------------------------------\n";
	cout << "black box example competition client in C++\n";
	cout << "-------------------------------------------\n";
	cout << "\n";

	// configure bbcomp
	if (configure(1, LOGFILEPATH) == 0) { printf("configure() failed: %s\n", errorMessage()); return; }

	// log into the system with the user data specified at the top
	safeLogin();
	cout << "successfully logged in as '" << USERNAME << "'" << endl;

	// request the tracks available to this user (this is optional)
	int numTracks = numberOfTracks();
	if (numTracks == 0) { cout << "numberOfTracks() failed: " << errorMessage() << endl; return; }
	cout << numTracks << " track(s):\n";
	for (int i=0; i<numTracks; i++)
	{
		const char* trackname = trackName(i);
		if (trackname == NULL) { cout << "trackName() failed: " << errorMessage() << endl; return; }
		cout << "  " << i << ": " << trackname << "\n";
	}

	// set the track specified at the top
	safeSetTrack();
	cout << "selected track '" << TRACKNAME << "'" << endl;

	// obtain the number of problems in the track
	int numProblems = safeGetNumberOfProblems();
	cout << "number of problems in the track: " << numProblems << endl;

	// For demonstration purposes we optimize only the first problem in the track.
	int problemID = 0;
	safeSetProblem(problemID);
	cout << "selected problem: " << problemID << endl;

	// obtain problem properties
	int dim = safeGetDimension(problemID);
	int obj = safeGetNumberOfObjectives(problemID);
	int bud = safeGetBudget(problemID);
	int evals = safeGetEvaluations(problemID);
	cout << "problem dimension: " << dim << "\n";
	cout << "number of objectives: " << obj << "\n";
	cout << "problem budget: " << bud << "\n";
	cout << "number of already used up evaluations: " << evals << "\n";

	// allocate memory for a search point
	vector<double> point(dim);
	vector<double> value(obj);

	// run the optimization loop
	srand(time(NULL));
	for (int e = 0; e < bud; e++)
	{
		if (e < evals)
		{
			// If evals > 0 then we have already optimized this problem to some point.
			// Maybe the optimizer crashed or was interrupted.
			//
			// This code demonstrates a primitive recovery approach, namely to replay
			// the history as if it were the actual black box queries. In this example
			// this affects only "bestValue" since random search does not have any
			// state variables.
			// As soon as e >= evals we switch over to black box queries.
			int result = history(e, &point[0], &value[0]);
			if (result == 0) { cout << "history() failed: " << errorMessage() << endl; return; }
		}
		else
		{
			// define a search point, here uniformly at random
			for (int d = 0; d < dim; d++) point[d] = (rand() + 0.5) / (RAND_MAX + 1.0);

			// query the black box
			safeEvaluate(problemID, &point[0], &value[0]);
		}

		// In any real algorithm "point" and "value" would update the internals state.
		// Here we just output the values.
		cout << "[" << e << "] f(" << point[0];
		for (int i=1; i<dim; i++) cout << "," << point[i];
		cout << ") = " << value[0];
		for (int i=1; i<obj; i++) cout << "," << value[i];
		cout << endl;
	}

	// check that we are indeed done
	evals = safeGetEvaluations(problemID);
	if (evals == bud) cout << "optimization finished.\n";
	else cout << "something went wrong: number of evaluations does not coincide with budget :(\n";
}


int main()
{
	// connect to black box library
	DLL_LOAD
	if (DLL_LOAD_STATUS < 0)
	{
		cout << "error loading library" << endl;
		exit(EXIT_FAILURE);
	}

	optimize();

	// quit cleanly
	DLL_CLOSE
}
