
from ctypes import *
from numpy.ctypeslib import ndpointer
from numpy import *
import sys
import platform
import time


# configuration
LOGFILEPATH = "logs/"
USERNAME = "demoaccount"
PASSWORD = "demopassword"
TRACKNAME = "trial"
# TRACKNAME = "trialMO"
LOGIN_DELAY_SECONDS = 10
LOCK_DELAY_SECONDS  = 60


# get library name
dllname = ""
if platform.system() == "Windows":
	dllname = "./bbcomp.dll"
elif platform.system() == "Linux":
	dllname = "./libbbcomp.so"
elif platform.system() == "Darwin":
	dllname = "./libbbcomp.dylib"
else:
	sys.exit("unknown platform")

# initialize dynamic library
bbcomp = CDLL(dllname)
bbcomp.configure.restype = c_int
bbcomp.login.restype = c_int
bbcomp.numberOfTracks.restype = c_int
bbcomp.trackName.restype = c_char_p
bbcomp.setTrack.restype = c_int
bbcomp.numberOfProblems.restype = c_int
bbcomp.setProblem.restype = c_int
bbcomp.dimension.restype = c_int
bbcomp.numberOfObjectives.restype = c_int
bbcomp.budget.restype = c_int
bbcomp.evaluations.restype = c_int
bbcomp.evaluate.restype = c_int
bbcomp.evaluate.argtypes = [ndpointer(c_double, flags="C_CONTIGUOUS"), ndpointer(c_double, flags="C_CONTIGUOUS")]
bbcomp.history.restype = c_int
bbcomp.history.argtypes = [c_int, ndpointer(c_double, flags="C_CONTIGUOUS"), ndpointer(c_double, flags="C_CONTIGUOUS")]
bbcomp.errorMessage.restype = c_char_p


# network failure resilient functions
def safeLogin():
	while True:
		result = bbcomp.login(USERNAME.encode('ascii'), PASSWORD.encode('ascii'))
		if result != 0:
			return
		msg = bbcomp.errorMessage().decode("ascii")
		print("WARNING: login failed: " + msg)
		time.sleep(LOGIN_DELAY_SECONDS)
		if msg == "already logged in":
			return

def safeSetTrack():
	while True:
		result = bbcomp.setTrack(TRACKNAME.encode('ascii'))
		if result != 0:
			return
		print("WARNING: setTrack failed: " + errorMessage().decode("ascii"))
		safeLogin()

def safeGetNumberOfProblems():
	while True:
		result = bbcomp.numberOfProblems()
		if result != 0:
			return result
		print("WARNING: numberOfProblems failed: " + errorMessage().decode("ascii"))
		safeSetTrack()

def safeSetProblem(problemID):
	while True:
		result = bbcomp.setProblem(problemID)
		if result != 0:
			return
		msg = bbcomp.errorMessage().decode("ascii")
		print("WARNING: evaluate failed: " + msg)
		if len(msg) >= 22 and msg[0:22] == "failed to acquire lock":
			time.sleep(LOCK_DELAY_SECONDS)
		else:
			safeSetTrack()

def safeEvaluate(problemID, point, objectives):
	while True:
		value = zeros(objectives)
		result = bbcomp.evaluate(point, value)
		if result != 0:
			if objectives == 1:
				return value[0]
			else:
				return value
		print("WARNING: evaluate failed: " + bbcomp.errorMessage().decode("ascii"))
		safeSetProblem(problemID)

def safeGetDimension(problemID):
	while True:
		result = bbcomp.dimension()
		if result != 0:
			return result
		print("WARNING: dimension failed: " + bbcomp.errorMessage().decode("ascii"))
		safeSetProblem(problemID)

def safeGetNumberOfObjectives(problemID):
	while True:
		result = bbcomp.numberOfObjectives()
		if result != 0: return result
		print("WARNING: numberOfObjectives failed: " + errorMessage().decode("ascii"))
		safeSetProblem(problemID)

def safeGetBudget(problemID):
	while True:
		result = bbcomp.budget()
		if result != 0: return result
		print("WARNING: budget failed: " + errorMessage().decode("ascii"))
		safeSetProblem(problemID)

def safeGetEvaluations(problemID):
	while True:
		result = bbcomp.evaluations()
		if result >= 0: return result
		print("WARNING: evaluations failed: " + errorMessage().decode("ascii"))
		safeSetProblem(problemID)


print("----------------------------------------------")
print("black box example competition client in Python")
print("----------------------------------------------")
print("")

# configure bbcomp
if bbcomp.configure(1, LOGFILEPATH.encode('ascii')) == 0:
	sys.exit("configure() failed: " + bbcomp.errorMessage().decode("ascii"))


# log into the system with the user data specified at the top
safeLogin()
print("successfully logged in as '" + USERNAME + "'")

# request the tracks available to this user (this is optional)
numTracks = bbcomp.numberOfTracks()
if numTracks == 0:
	sys.exit("numberOfTracks() failed: " + bbcomp.errorMessage().decode("ascii"))

print(str(numTracks) + " track(s):")
for i in range(numTracks):
	trackname = bbcomp.trackName(i).decode("ascii")
	if bool(trackname) == False:
		sys.exit("trackName() failed: " + bbcomp.errorMessage().decode("ascii"))

	print("  " + str(i) + ": " + trackname)

# set the track specified at the top
safeSetTrack()
print("selected track '" + TRACKNAME + "'")

# obtain the number of problems in the track
numProblems = safeGetNumberOfProblems()
print("number of problems in the track: " + str(numProblems))

# For demonstration purposes we optimize only the first problem in the track.
problemID = 0
safeSetProblem(problemID)
print("selected problem: " + str(problemID))

# obtain problem properties
dim = safeGetDimension(problemID)
obj = safeGetNumberOfObjectives(problemID)
bud = safeGetBudget(problemID)
evals = safeGetEvaluations(problemID)
print("problem dimension: " + str(dim))
print("number of objectives: " + str(obj))
print("problem budget: " + str(bud))
print("number of already used up evaluations: " + str(evals))

# allocate memory for a search point
point = zeros(dim)
value = zeros(obj)

# run the optimization loop
for e in range(bud):
	if e < evals:
		# If evals > 0 then we have already optimized this problem to some point.
		# Maybe the optimizer crashed or was interrupted.
		#
		# This code demonstrates a primitive recovery approach, namely to replay
		# the history as if it were the actual black box queries. In this example
		# this affects only "bestValue" since random search does not have any
		# state variables.
		# As soon as e >= evals we switch over to black box queries.
		result = bbcomp.history(e, point, value)
		if result == 0:
			sys.exit("history() failed: " + bbcomp.errorMessage().decode("ascii"))
	else:
		# define a search point, here uniformly at random
		point = random.rand(dim)

		# query the black box
		value = safeEvaluate(problemID, point, obj)

	# In any real algorithm "point" and "value" would update the internals state.
	# Here we just output the values.
	print('[{0}] f({1}) = {2}'.format(e, point, value))

# check that we are indeed done
evals = safeGetEvaluations(problemID)
if evals == bud:
	print("optimization finished.")
else:
	print("something went wrong: number of evaluations does not coincide with budget :(")
