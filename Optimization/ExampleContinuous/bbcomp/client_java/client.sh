#!/bin/bash 

if [ ! -f ../proxy/proxy ]
then
	echo ERROR: proxy executable not found.
	echo The Java client depends on the BBComp TCP/IP proxy.
	echo Please enter the proxy directory and build the program
	echo with the makefile suitable for your operating system.
	echo Then try again.
	exit
fi

cp -n ../library/bbcomp.dll      . 2>/dev/null || :
cp -n ../library/libbbcomp.so    . 2>/dev/null || :
cp -n ../library/libbbcomp.dylib . 2>/dev/null || :
cp -n ../proxy/proxy .

javac BBComp.java Client.java
set -m
./proxy &
sleep 1s
java Client
kill %1
