import java.io.*;
import java.net.*;
import java.util.*;

public class Client
{
	// configuration
	public static final String LOGFILEPATH = "logs/";
	public static final String USERNAME = "demoaccount";
	public static final String PASSWORD = "demopassword";
	public static final String TRACKNAME = "trial";
	// public static final String TRACKNAME = "trialMO";
	public static final int LOGIN_DELAY_SECONDS  = 10;  // 10 seconds
	public static final int LOCK_DELAY_SECONDS   = 60;  // 60 seconds
	public static BBComp bbcomp;

	public static void main(String args[])
	{
		try
		{
			// configure bbcomp
			setupBBComp();

			// log into the system with the user data specified at the top
			safeLogin();
			System.out.format("successfully logged in as '%s'%n", USERNAME);

			// request the tracks available to this user (this is optional)
			int numTracks = bbcomp.numberOfTracks();
			if (numTracks == 0)
			{
				System.out.println("numberOfTracks() failed: " + bbcomp.errorMessage());
				System.exit(1);
			}

			System.out.format("%d track(s):%n", numTracks);
			for (int i=0; i<numTracks; i++)
			{
				String trackname = bbcomp.trackName(i);
				if (trackname.equals(""))
				{
					System.out.println("trackName() failed: " + bbcomp.errorMessage());
					System.exit(1);
				}
				System.out.format("  %d: %s%n", i, trackname);
			}

			// set the track specified at the top
			safeSetTrack();
			System.out.format("selected track '%s'%n", TRACKNAME);

			// obtain the number of problems in the track
			int numProblems = safeGetNumberOfProblems();
			System.out.format("number of problems in the track: %d%n", numProblems);

			// For demonstration purposes we optimize only the first problem in the track.
			int problemID = 0;
			safeSetProblem(problemID);
			System.out.format("selected problem: %d%n", problemID);

			// obtain problem properties
			int dim = safeGetDimension(problemID);
			int obj = safeGetNumberOfObjectives(problemID);
			int bud = safeGetBudget(problemID);
			int evals = safeGetEvaluations(problemID);
			System.out.format("problem dimension: %d%n", dim);
			System.out.format("number of objectives: %d%n", obj);
			System.out.format("problem budget: %d%n", bud);
			System.out.format("number of already used up evaluations: %d%n", evals);

			// allocate memory for a search point
			Vector<Double> point = new Vector<Double>(dim);
			Vector<Double> value = new Vector<Double>(obj);
			point.setSize(dim);
			value.setSize(obj);

			// run the optimization loop
			for (int e=0; e<bud; e++)
			{
				if (e < evals)
				{
					// If evals > 0 then we have already optimized this problem to some point.
					// Maybe the optimizer crashed or was interrupted.
					//
					// This code demonstrates a primitive recovery approach, namely to replay
					// the history as if it were the actual black box queries. In this example
					// this affects only "bestValue" since random search does not have any
					// state variables.
					// As soon as e >= evals we switch over to black box queries.
					BBComp.HistoryReturn hr = bbcomp.history(e);
					if (! hr.result)
					{
						System.out.println("history() failed: " + bbcomp.errorMessage());
						System.exit(1);
					}
					point = hr.point;
					value = hr.value;
				}
				else
				{
					// define a search point, here uniformly at random
					for (int d=0; d<dim; d++) point.setElementAt(Math.random(), d);

					// query the black box
					value = safeEvaluate(problemID, point);
				}

				// In any real algorithm "point" and "value" would update the internals state.
				// Here we just output the values.
				System.out.print("[" + e + "] f(" + point.get(0));
				for (int i=1; i<dim; i++) System.out.print("," + point.get(i));
				System.out.print(") = " + value.get(0));
				for (int i=1; i<obj; i++) System.out.print("," + value.get(i));
				System.out.println();
			}

			// check that we are indeed done
			evals = safeGetEvaluations(problemID);
			if (evals == bud) System.out.println("optimization finished.");
			else System.out.println("something went wrong: number of evaluations does not coincide with budget :(");
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		finally
		{
		}
	}

	private static void setupBBComp()
	{
		while (true)
		{
			try
			{
				bbcomp = null;

				// connect to black box library
				bbcomp = new BBComp();

				// setup
				if (! bbcomp.configure(true, LOGFILEPATH))
				{
					System.out.println("configure() failed: " + bbcomp.errorMessage());
					System.exit(1);
				}
				return;
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}
	}
	
	private static void safeLogin()
	{
		while (true)
		{
			String msg = new String();
			try	{
				boolean result = bbcomp.login(USERNAME, PASSWORD);
				if (result) return;
				msg = bbcomp.errorMessage();
				System.out.format("WARNING: login failed: %s%n", msg);	
			} catch (Exception ex)         { System.out.println(ex.getMessage()); setupBBComp(); }
			if (msg.equals("already logged in")) return;
			try                            { Thread.sleep(LOGIN_DELAY_SECONDS*1000); } 
			catch(InterruptedException ex) { Thread.currentThread().interrupt(); }
		}
	}

	private static void safeSetTrack()
	{
		while (true)
		{
			try	{
				boolean result = bbcomp.setTrack(TRACKNAME);
				if (result) return;
				System.out.format("WARNING: setTrack failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeLogin();
		}
	}

	private static int safeGetNumberOfProblems()
	{
		while (true)
		{
			try	{
				int result = bbcomp.numberOfProblems();
				if (result != 0) return result;
				System.out.format("WARNING: numberOfProblems failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetTrack();			
		}
	}

	private static void safeSetProblem(int problemID)
	{
		while (true)
		{
			String msg = new String();
			try	{
				boolean result = bbcomp.setProblem(problemID);
				if (result) return;
				msg = bbcomp.errorMessage();
				System.out.format("WARNING: setProblem failed: %s%n", msg);
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			if (msg.length() >= 22 && msg.substring(0, 22).equals("failed to acquire lock")) {
				try                            { Thread.sleep(LOCK_DELAY_SECONDS*1000); } 
				catch(InterruptedException ex) { Thread.currentThread().interrupt(); }
			}
			else safeSetTrack();
		}
	}

	private static Vector<Double> safeEvaluate(int problemID, Vector<Double> point)
	{
		while (true)
		{
			try	{
				BBComp.EvaluateReturn result = bbcomp.evaluate(point);
				if (result.result) return result.value;
				System.out.format("WARNING: evaluate failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetProblem(problemID);
		}
	}

	private static int safeGetDimension(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.dimension();
				if (result != 0) return result;
				System.out.format("WARNING: dimension failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetProblem(problemID);			
		}
	}

	private static int safeGetNumberOfObjectives(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.numberOfObjectives();
				if (result != 0) return result;
				System.out.format("WARNING: numberOfObjectives failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetProblem(problemID);			
		}
	}

	private static int safeGetBudget(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.budget();
				if (result != 0) return result;
				System.out.format("WARNING: budget failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetProblem(problemID);			
		}
	}

	private static int safeGetEvaluations(int problemID)
	{
		while (true)
		{
			try	{
				int result = bbcomp.evaluations();
				if (result >= 0) return result;
				System.out.format("WARNING: evaluations failed: %s%n", bbcomp.errorMessage());
			} catch (Exception ex) { System.out.println(ex.getMessage()); setupBBComp(); }
			safeSetProblem(problemID);			
		}
	}
}
