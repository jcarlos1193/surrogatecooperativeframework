function client
    % configuration
    global LIBNAME; global LIBHFILE;    
    global TRACKNAME;   global USERNAME; global PASSWORD; 
    global LOGIN_DELAY_SECONDS, global LOCK_DELAY_SECONDS;
    LIBNAME = 'bbcomp'; % working directory should include library file(s) and header file
    LIBHFILE = 'bbcomplib.h';   
    LOGFILEPATH = 'logs/';  % path to log files, make sure that it exists and is writable!
    USERNAME = 'demoaccount';
    PASSWORD = 'demopassword';
    TRACKNAME = 'trial';
    // TRACKNAME = 'trialMO';
    LOGIN_DELAY_SECONDS = 10;
    LOCK_DELAY_SECONDS = 60;

    loadBBCompLibrary();

    disp('---------------------------------------------------');
    disp('black box full example competition client in Matlab');
    disp('---------------------------------------------------');
    disp(' ');

    % working directory should include library file(s) and header file
	% for Windows platforms: bbcomp.dll (64 bit), msvcr120.dll, msvcp120.dll
    libname = 'bbcomp';
    if (libisloaded(libname) == 1)
        unloadlibrary(libname);
    end;
    loadlibrary(libname,'bbcomplib.h', 'mfilename', 'mHeader');
    Library_Functions = libfunctions(libname, '-full') % print library functions

	% configure bbcomp
    if (calllib(libname,'configure', 1, LOGFILEPATH) == 0) whenFailed('configure', libname); return; end;

	% log into the system with the user data specified at the top
	safeLogin();
	disp(['successfully logged in as ' USERNAME]);

    % request the tracks available to this user (this is optional)
    printTrackNames();

	% set the track specified at the top
	safeSetTrack();
    disp(['track set to "' TRACKNAME '"']);

	% obtain the number of problems in the track
	int numProblems = safeGetNumberOfProblems();
	disp(['number of problems in the track: ' numProblems]);

	% For demonstration purposes we optimize only the first problem in the track.
	problemID = 0;
	safeSetProblem(problemID);
	disp(['selected problem: ' problemID]);

    % obtain problem properties
    bud = safeGetBudget(problemID);
    dim = safeGetDimension(problemID);
    obj = safeGetNumberOfObjectives(problemID);
    evals = safeGetEvaluations(problemID);
    disp(['problem dimension: ' num2str(dim)]);
    disp(['number of objectives: ' num2str(obj)]);
    disp(['problem budget: ' num2str(bud)]);
    disp(['number of already used up evaluations: ' num2str(evals)]);

    % allocate memory for a search point and function value
    point = zeros(1,dim);
    value = zeros(1,obj);

    % run the optimization loop
    rand('state',123);
    for e=1:bud
        if (e <= evals)
            % If evals > 0 then we have already optimized this problem to some point.
            % Maybe the optimizer crashed or was interrupted.
            %
            % This code demonstrates a primitive recovery approach, namely to replay
            % the history as if it were the actual black box queries. In this example
            % this affects only "bestValue" since random search does not have any
            % state variables.
            % As soon as e >= evals we switch over to black box queries.
            value = libpointer('doublePtr',zeros(1,obj));
            point = libpointer('doublePtr',zeros(1,dim));
        	[result, point, value] = calllib(libname,'history',e-1,point,value);
        	if (result == 0) whenFailed('history', libname);    end;
        else
            % define a search point, here uniformly at random
            for d=1:dim
                point(d) = rand();
            end;
            % query the black box
            value = safeEvaluate(problemID, point)
        end;

        % In any real algorithm "point" and "value" would update the internals state.
        sx = sprintf('%d ', point);
        sf = sprintf('%d', value);
        disp(['[' num2str(e) '] f(' sx ') = ' sf]);
    end;

    % check that we are indeed done
    evals = safeGetEvaluations(problemID);
    if (evals == 0) whenFailed('evaluations', libname);    return;	end;
    if (evals == bud)   disp('optimization finished.');
    else                disp('something went wrong: number of evaluations does not coincide with budget :(');   end;

    end;

    unloadlibrary(libname);

function whenFailed(funcname, LIBNAME)
    disp([funcname '() failed: ' calllib(LIBNAME,'errorMessage')]);    
    unloadlibrary(LIBNAME);

function loadBBCompLibrary()
    global LIBNAME; global LIBHFILE;
    if (libisloaded(LIBNAME) == 1) % first unload if already loaded
        unloadlibrary(LIBNAME);
    end;
    loadlibrary(LIBNAME,LIBHFILE);
    Library_Functions = libfunctions(LIBNAME, '-full') % print library functions
    if (numel(Library_Functions) == 0)
        disp('error loading library');
        return;
    end;

function safeLogin()
    global LIBNAME; global USERNAME; global PASSWORD; global LOGIN_DELAY_SECONDS;
    while (1)
        result = calllib(LIBNAME,'login',USERNAME,PASSWORD); 
        if (result ~= 0) return;    end;
        msg = calllib(LIBNAME,'errorMessage');
        disp(['WARNING: login failed: ' msg]);
        pause(LOGIN_DELAY_SECONDS);
        if (strcmp(msg, 'already logged in') == 0) return;  end;
    end
    
function safeSetTrack()
    global TRACKNAME; global LIBNAME; global USERNAME; 
    global PASSWORD; global LOGIN_DELAY_SECONDS;
    while (1)
        result = calllib(LIBNAME,'setTrack',TRACKNAME);
        if (result ~= 0) return;    end;
        disp(['WARNING: setTrack failed: ' calllib(LIBNAME,'errorMessage')]);
        safeLogin();
    end;
    
function printTrackNames()
    global LIBNAME;
    numTracks = calllib(LIBNAME,'numberOfTracks');     
    if (numTracks == 0) whenFailed('numberOfTracks', LIBNAME);    return;	end;
    disp([num2str(numTracks) ' track(s):']);
    for i=1:numTracks
        trackname = calllib(LIBNAME,'trackName',i-1); % indexing from 0
        if (isempty(trackname))         whenFailed('trackName', LIBNAME);       return;
        else                            disp([' ' num2str(i) ': ' trackname]);	end;
    end;

function numProblems = safeGetNumberOfProblems()
    global LIBNAME;
    numProblems = calllib(LIBNAME,'numberOfProblems');
    if (numProblems == 0) whenFailed('numberOfProblems', LIBNAME);     return;	end;
    disp(['The track consists of ' num2str(numProblems) ' problems.']);

function safeSetProblem(problemID)
    global LIBNAME; global LOCK_DELAY_SECONDS;
    while (1)
        result = calllib(LIBNAME,'setProblem',problemID);
        if (result ~= 0) return;    end;
        msg = calllib(LIBNAME,'errorMessage');
        disp(['WARNING: setProblem failed: ' msg]);
        %if (strcmp(msg, 'failed to acquire lock') == 0)
            pause(LOCK_DELAY_SECONDS);
       % else
            safeSetTrack();
       % end; 
    end;

function result = safeGetDimension(problemID)
    global LIBNAME;
    while (1)
        result = calllib(LIBNAME,'dimension');
        if (result ~= 0) return;    end;
        disp(['WARNING: dimension failed: ' calllib(LIBNAME,'errorMessage')]);
        safeSetProblem(problemID);
    end;

function result = safeGetNumberOfObjectives(problemID)
    global LIBNAME;
    while (1)
        result = calllib(LIBNAME,'numberOfObjectives');
        if (result ~= 0) return;    end;
        disp(['WARNING: numberOfObjectives failed: ' calllib(LIBNAME,'errorMessage')]);
        safeSetProblem(problemID);
    end;

function result = safeGetBudget(problemID)
    global LIBNAME;
    while (1)
        result = calllib(LIBNAME,'budget');
        if (result ~= 0) return; end;
        disp(['WARNING: budget failed: ' calllib(LIBNAME,'errorMessage')]);
        safeSetProblem(problemID);
    end;

function result = safeGetEvaluations(problemID)
    global LIBNAME;
    while (1)
        result = calllib(LIBNAME,'evaluations');
        if (result >= 0) return;    end;
        disp(['WARNING: evaluations failed: ' calllib(LIBNAME,'errorMessage')]);
        safeSetProblem(problemID);
    end;

function value = safeEvaluate(problemID, point)
    global LIBNAME;
	while (1)
        % query the black box
        value = libpointer('doublePtr',1e+100);
        [result, TMP, value] = calllib(LIBNAME,'evaluate',point,value);
		if (result ~= 0) return;    end;
		disp(['WARNING: evaluate failed: ' calllib(LIBNAME,'errorMessage')]);
		safeSetProblem(problemID);
    end;

function value = truncate2bounds(value)
    value = max(0.0, min(1.0, value));
