
// black box library header and function (pointer) declarations
#include <bbcomplib.h>
DLL_DECLARATIONS

// additional headers
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


// configuration
#define LOGFILEPATH "logs/"
#define USERNAME "demoaccount"
#define PASSWORD "demopassword"
#define TRACKNAME "trial"               // single-objective trail track
//#define TRACKNAME "trialMO"             // multi-objective trail track
#define LOGIN_DELAY_USECONDS  10000000  // 10 seconds
#define LOCK_DELAY_USECONDS   60000000  // 60 seconds


///////////////////////////////////////////////////////////
// auxiliary functions
//

#ifndef _WIN32
#include <unistd.h>
#endif

// sleep in microseconds
void u_sleep(long usec)
{
#ifdef _WIN32
	HANDLE timer;
	LARGE_INTEGER ft;
	ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time
	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);
#else
	usleep(usec);
#endif
}


///////////////////////////////////////////////////////////
// network failure resilient functions
//

void safeLogin()
{
	while (1)
	{
		int result = login(USERNAME, PASSWORD);
		if (result != 0) return;
		const char* msg = errorMessage();
		printf("WARNING: login failed: %s\n", msg);
		u_sleep(LOGIN_DELAY_USECONDS);
		if (strcmp(msg, "already logged in") == 0) return;
	}
}

void safeSetTrack()
{
	while (1)
	{
		int result = setTrack(TRACKNAME);
		if (result != 0) return;
		printf("WARNING: setTrack failed: %s\n", errorMessage());
		safeLogin();
	}
}

int safeGetNumberOfProblems()
{
	while (1)
	{
		int result = numberOfProblems();
		if (result != 0) return result;
		printf("WARNING: numberOfProblems failed: %s\n", errorMessage());
		safeSetTrack();
	}
}

void safeSetProblem(int problemID)
{
	while (1)
	{
		int result = setProblem(problemID);
		if (result != 0) return;
		const char* msg = errorMessage();
		printf("WARNING: setProblem failed: %s\n", msg);
		if (memcmp(msg, "failed to acquire lock", 22) == 0) u_sleep(LOCK_DELAY_USECONDS);
		else safeSetTrack();
	}
}

void safeEvaluate(int problemID, double* point, double* value)
{
	while (1)
	{
		int result = evaluate(point, value);
		if (result != 0) return;
		printf("WARNING: evaluate failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetDimension(int problemID)
{
	while (1)
	{
		int result = dimension();
		if (result != 0) return result;
		printf("WARNING: dimension failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetNumberOfObjectives(int problemID)
{
	while (1)
	{
		int result = numberOfObjectives();
		if (result != 0) return result;
		printf("WARNING: numberOfObjectives failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetBudget(int problemID)
{
	while (1)
	{
		int result = budget();
		if (result != 0) return result;
		printf("WARNING: budget failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}

int safeGetEvaluations(int problemID)
{
	while (1)
	{
		int result = evaluations();
		if (result >= 0) return result;
		printf("WARNING: evaluations failed: %s\n", errorMessage());
		safeSetProblem(problemID);
	}
}


// This example client program implements random search
// to optimize the first problem in the "trial" track.
void optimize()
{
	printf("-----------------------------------------\n");
	printf("black box example competition client in C\n");
	printf("-----------------------------------------\n");
	printf("\n");

	// configure bbcomp
	if (configure(1, LOGFILEPATH) == 0) { printf("configure() failed: %s\n", errorMessage()); return; }

	// log into the system with the user data specified at the top
	safeLogin();
	printf("successfully logged in as '%s'\n", USERNAME);

	// list the tracks available to this user (this is optional)
	int numTracks = numberOfTracks();
	if (numTracks == 0) { printf("numberOfTracks() failed: %s\n", errorMessage()); return; }
	printf("%d track(s):\n", numTracks);
	for (int i=0; i<numTracks; i++)
	{
		const char* trackname = trackName(i);
		if (trackname == NULL) { printf("trackName() failed: %s\n", errorMessage()); return; }
		printf("  %d: %s\n", i, trackname);
	}

	// set the track specified at the top
	safeSetTrack();
	printf("selected track '%s'\n", TRACKNAME);

	// obtain the number of problems in the track
	int numProblems = safeGetNumberOfProblems();
	printf("number of problems in the track: %d\n", numProblems);

	// For demonstration purposes we optimize only the first problem in the track.
	int problemID = 0;
	safeSetProblem(problemID);
	printf("selected problem: %d\n", problemID);

	// obtain problem properties
	int dim = safeGetDimension(problemID);
	int obj = safeGetNumberOfObjectives(problemID);
	int bud = safeGetBudget(problemID);
	int evals = safeGetEvaluations(problemID);
	printf("problem dimension: %d\n", dim);
	printf("number of objectives: %d\n", obj);
	printf("problem budget: %d\n", bud);
	printf("number of already used up evaluations: %d\n", evals);

	// allocate memory for a search point
	double* point = (double*)malloc(dim * sizeof(double));
	if (point == NULL) { printf("out of memory\n"); return; }
	double* value = (double*)malloc(obj * sizeof(double));
	if (value == NULL) { printf("out of memory\n"); return; }

	// run the optimization loop
	srand(time(NULL));
	for (int e=0; e<bud; e++)
	{
		if (e < evals)
		{
			// If evals > 0 then we have already optimized this problem to some point.
			// Maybe the optimizer crashed or was interrupted.
			//
			// This code demonstrates a primitive recovery approach, namely to replay
			// the history as if it were the actual black box queries. In this example
			// this affects only "bestValue" since random search does not have any
			// state variables.
			// As soon as e >= evals we switch over to black box queries.
			int result = history(e, point, value);
			if (result == 0) { printf("history() failed: %s\n", errorMessage()); return; }
		}
		else
		{
			// define a search point, here uniformly at random
			for (int d=0; d<dim; d++) point[d] = (rand() + 0.5) / (RAND_MAX + 1.0);

			// query the black box
			safeEvaluate(problemID, point, value);
		}

		// In any real algorithm "point" and "value" would update the internals state.
		// Here we just output the values.
		printf("[%d] f(%g", e, point[0]);
		for (int i=1; i<dim; i++) printf(",%g", point[i]);
		printf(") = %g", value[0]);
		for (int i=1; i<obj; i++) printf(",%g", value[i]);
		printf("\n");
	}

	free(point);
	free(value);

	// check that we are indeed done
	evals = safeGetEvaluations(problemID);
	if (evals == bud) printf("optimization finished.\n");
	else printf("something went wrong: number of evaluations does not coincide with budget :(\n");
}


int main()
{
	// connect to black box library
	DLL_LOAD
	if (DLL_LOAD_STATUS < 0)
	{
		printf("error loading library\n");
		exit(EXIT_FAILURE);
	}

	optimize();

	// quit cleanly
	DLL_CLOSE
}
