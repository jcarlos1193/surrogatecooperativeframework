function Data = datainput_bbob(function_id)

global g_function_id;

if nargin < 1
    function_id = g_function_id;
end

handles = benchmarks('handles');
function_handle = handles{function_id};

Data.dim = 10; %problem dimension
Data.xlow = -5 * ones(1,Data.dim); %variable lower bounds
Data.xup = 5 * ones(1,Data.dim); %variable upper bounds

%objective function
Data.objfunction = @(x) function_handle(x', Data.dim);

[fopt, xopt] = function_handle('xopt', Data.dim);
Data.known_best_f = fopt;
Data.known_best_x = xopt;

Data.integer = []; %indices of integer variables
Data.continuous = (1:Data.dim); %indices of continuous variables

end

% % SEPARABLE
% 1 Sphere
% 2 Ellipsoid separable with monotone x-transformation, condition 1e6
% 3 Rastrigin separable with both x-transformations "condition" 10
% 4 Skew Rastrigin-Bueche separable, "condition" 10, skew-"condition" 100
% 5 Linear slope, neutral extension outside the domain (not flat)
% 
% % LOW OR MODERATE CONDITION
% 6 Attractive sector function
% 7 Step-ellipsoid, condition 100
% 8 Rosenbrock, original
% 9 Rosenbrock, rotated
% 
% % HIGH CONDITION
% 10 Ellipsoid with monotone x-transformation, condition 1e6
% 11 Discus with monotone x-transformation, condition 1e6
% 12 Bent cigar with asymmetric x-transformation, condition 1e6
% 13 Sharp ridge, slope 1:100, condition 10
% 14 Sum of different powers
% 
% % MULTI-MODAL
% 15 Rastrigin with both x-transformations, condition 10
% 16 Weierstrass with monotone x-transformation, condition 100
% 17 Schaffer F7 with asymmetric x-transformation, condition 10
% 18 Schaffer F7 with asymmetric x-transformation, condition 1000
% 19 F8F2 composition of 2-D Griewank-Rosenbrock
% 
% % MULTI-MODAL WITH WEAK GLOBAL STRUCTURE
% 20 Schwefel x*sin(x) with tridiagonal transformation, condition 10
% 21 Gallagher 101 Gaussian peaks, condition up to 1000
% 22 Gallagher 21 Gaussian peaks, condition up to 1000, 1000 for global opt
% 23 Katsuuras repetitive rugged function
% 24 Lunacek bi-Rastrigin, condition 100