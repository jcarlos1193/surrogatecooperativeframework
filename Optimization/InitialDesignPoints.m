function [sample_x, sample_y] = InitialDesignPoints(initial_design, number_startpoints, lower_bounds, upper_bounds, fun, starting_point)

Data.dim = length(lower_bounds);
Data.xup = upper_bounds;
Data.xlow = lower_bounds;

% Generate initial experimental design
sample_x = StartingDesign(initial_design, number_startpoints, Data);

% User gives one or more starting points to add to the initital design
if exist('starting_point', 'var')
    sample_x = [starting_point; sample_x];
end

% Regenerate initial experimental design if rank of sample site matrix too low
while rank([sample_x, ones(size(sample_x,1),1)]) < Data.dim + 1 % regenerate starting design if rank of matrix too small
    
    sample_x = StartingDesign(initial_design, number_startpoints, Data);
    
    if exist ('starting_point', 'var')
        sample_x = [starting_point; sample_x];
    end
    
end

% Do expensive function evaluations at points in initial experimental design
sample_y = zeros(size(sample_x,1),1);

for ii = 1 : size(sample_x,1)
    sample_y(ii,1) = fun(sample_x(ii,:));
end 

end