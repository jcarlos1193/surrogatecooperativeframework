clear;
clc;
addpath(genpath('..\..\Optimization'));

func_names = {'branin', 'goldprice', 'shekel5', 'shekel7', 'shekel10', 'hartman3', 'hartman6'};
num_processors = [4, 8, 12];
max_iters = 100;
num_replicas = 20;
algs_name = {'CORS_and_PEI', 'PEI', 'CORS'};

for alg = algs_name
    
    for qpoints = num_processors

        for func = func_names

            switch func{1}
                case 'branin'
                    str_file = 'datainput_Branin';
                case 'goldprice'
                    str_file = 'datainput_goldprice';
                case 'shekel5'
                    str_file = 'datainput_Shekel5';
                case 'shekel7'
                    str_file = 'datainput_Shekel7';
                case 'shekel10'
                    str_file = 'datainput_Shekel10';
                case 'hartman3'
                    str_file = 'datainput_hartman3';
                case 'hartman6'
                    str_file = 'datainput_hartman6';
            end

            Data = feval(str_file);
            
            func_name = ['..\Results\Res_', alg{1}, '_q', num2str(qpoints), '_', func{1}];
            number_startpoints = 2 * (Data.dim + 1); %ceil( (Data.dim+1)*(Data.dim+2) / (2*qpoints) ) * qpoints;
            
            if isfield(Data, 'stop_fvalue')
                value_to_reach = Data.stop_fvalue;
            else
                value_to_reach = Data.known_best_f + abs(Data.known_best_f) * 0.01;
            end 

            for idx_r = 1 : num_replicas
                
                str_init_design = ['..\Initial_points\initial_points_', func{1}, '_replica_', num2str(idx_r), '.mat'];
            
                if isfile(str_init_design) 
                    
                    load(str_init_design, 'starting_points');
                    
                else
                    
                    starting_points = StartingDesign('SLHD', number_startpoints, Data); 
                    
                    while rank( [starting_points, ones(size(starting_points,1), 1)] ) < Data.dim + 1 %regenerate starting design if rank of matrix too small
                        
                        starting_points = StartingDesign('SLHD', number_startpoints, Data);  %create initial experimental design
                        disp('regenerate')
                        
                    end
                    
                    save(str_init_design, 'starting_points');
                    
                end
                
                Data = SurrogateModelModule_v1(str_file, max_iters, 'RBFtps', alg{1}, 'SLHD', number_startpoints, starting_points, func_name, value_to_reach, qpoints);
                fprintf('Tiempo total: %f (s)\n', Data.TotalTime);
                
            end

        end

    end

end
