clear;
clc;
addpath(genpath('..\..\Optimization'));

global g_function_id;

str_file = 'datainput_bbob';
num_processors = [4, 8, 12];
max_iters = 100;
num_replicas = 20;
algs_name = {'CORS_and_PEI', 'PEI', 'CORS'};

for alg = algs_name
    
    for qpoints = num_processors

        for idx_func = 15 : 24

            g_function_id = idx_func;
            Data = feval(str_file);
            
            func_name = ['..\Results\Res_', alg{1}, '_q', num2str(qpoints), '_f', num2str(idx_func)];
            number_startpoints = 2 * (Data.dim + 1); %ceil( (Data.dim+1)*(Data.dim+2) / (2*qpoints) ) * qpoints;
            
            if isfield(Data, 'stop_fvalue')
                value_to_reach = Data.stop_fvalue;
            else
                value_to_reach = -Inf; %Data.known_best_f + abs(Data.known_best_f) * 0.01;
            end 

            for idx_r = 1 : num_replicas
                
                str_init_design = ['..\Initial_points\initial_points_f', num2str(idx_func), '_replica_', num2str(idx_r), '.mat'];
            
                if isfile(str_init_design) 
                    
                    load(str_init_design, 'starting_points');
                    
                else
                    
                    starting_points = StartingDesign('SLHD', number_startpoints, Data); 
                    
                    while rank( [starting_points, ones(size(starting_points,1), 1)] ) < Data.dim + 1 %regenerate starting design if rank of matrix too small
                        
                        starting_points = StartingDesign('SLHD', number_startpoints, Data);  %create initial experimental design
                        disp('regenerate')
                        
                    end
                    
                    save(str_init_design, 'starting_points');
                    
                end
                
                Data = SurrogateModelModule_v1(str_file, max_iters, 'RBFtps', alg{1}, 'SLHD', number_startpoints, starting_points, func_name, value_to_reach, qpoints);
                fprintf('Tiempo total: %f (s)\n', Data.TotalTime);
                
            end

        end

    end

end
