function starting_points = load_par(name_function, qpoints)
    load(['Initial_points/initial_points_', name_function, '_q', num2str(qpoints)], 'starting_points');
end
